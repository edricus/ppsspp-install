#!/bin/bash
##########
# Checks #
##########
[[ $(id -u) == 0 ]] && echo "Do not run as root, this script include compiling using make" && exit 1
if [[ $(lsb_release -ar 2>/dev/null | grep -i 'Distributor ID' | cut -s -f2) != 'Debian' ]]; then
    cat << EOF
This script is only for Debian
If you're using Ubuntu try adding ppsspp PPA : https://launchpad.net/~ppsspp/+archive/ubuntu/stable
EOF
exit 1
fi
#############
# Functions #
#############
CLONE(){
if [[ ! -d ppsspp ]]; then 
    git clone --recurse-submodules https://github.com/hrydgard/ppsspp.git
    cd ppsspp
else
    cd ppsspp
fi 
}
INSTALL(){
CLONE
sudo apt-get install -yqq python3 build-essential cmake libgl1-mesa-dev libsdl2-dev libvulkan-dev libglfw3-dev libgl1-mesa-dev libglu1-mesa-dev
cmake . 
make -j $(nproc)
echo 'Installing...'
sudo make install
[[ $? == 0 ]] &&\
echo "PPSSPP has been successfully installed" ||\
echo "Error occured while installing ppsspp"
exit
}
UPDATE(){
CLONE
git pull --rebase https://github.com/hrydgard/ppsspp.git
git submodule update --init --recursive
make -j $(nproc)
echo 'Updating...'
sudo make install
[[ $? == 0 ]] &&\
echo "PPSSPP has been successfully updated" ||\
echo "Error occured while updating ppsspp"
exit
}
UNINSTALL(){
echo 'Uninstalling...'
xargs sudo rm -rf <install-manifest.txt 
[[ $? == 0 ]] &&\
echo "PPSSPP has been successfully uninstalled" ||\
echo "Error occured while uninstalling ppsspp"
exit
}
###########
# Options #
###########
read -r -d '' HELP << EOF
Usage: ./ppsspp-install.sh [arguments]
Arguments:
    -i --install        Download, compile and install PPSSPP
    -r --remove         Uninstall PPSSPP
    -u --update         Update PPSSPP
    -h --help           Print help (this page)
EOF
case $1 in
    -i|--install )  INSTALL;;
    -r|--remove )   UNINSTALL;;
    -u|--update )   UPDATE;;
    -h|--help|* ) echo "$HELP" && exit;;
esac
if [ $# -ge 2 ]; then
    printf "use only 1 argument/n$HELP/n"
fi
