![ppsspp logo](https://em.lbbcdn.com/wp-content/uploads/2016/08/ppsspp-emu-logo-transparent.png "PPSSPP logo")

<h1> PPSSPP Debian - Compile and install PPSSPP on Debian </h1>
<h2> Usage </h2>

    ./ppsspp-install.sh [arguments]

<h2> Arguments </h2>

| Short | Long  | Description                          |
|---|-----------|--------------------------------------|
|-i | --install | Download, compile and install PPSSPP |
|-r | --remove  | Uninstall PPSSPP                     |
|-u | --update  | Update PPSSPP                        |
|-h | --help    | Print help (this page)               |


